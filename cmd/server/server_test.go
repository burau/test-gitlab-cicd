// +build integration

package main

import (
	"bytes"
	"fmt"
	"net"
	"net/http"
	"os"
	"strings"
	"testing"
	"time"
)

func Test_main(t *testing.T) {
	baseURL := setupServer()

	url := baseURL + "/hello"
	resp, err := http.Get(url)
	if err != nil {
		t.Errorf("Got unexpected error: error %q", err)
		return
	}

	buf := new(bytes.Buffer)
	_, _ = buf.ReadFrom(resp.Body)

	expected := "{\"message\":\"Hello World of CI/CD\"}"
	result := buf.String()

	if result != expected {
		t.Errorf("Got unexpected result: result %q, got %q\n", result, expected)
		return
	}

}

func setupServer() string {
	port, err := getFreePort()
	if err != nil {
		panic(err)
	}

	os.Setenv("HTTP_PORT", fmt.Sprint(port))
	go main()

	url := fmt.Sprintf("http://127.0.0.1:%d", port)

	if err := waitForServer(url, 5*time.Second); err != nil {
		panic(err)
	}

	return url
}

func waitForServer(url string, timeout time.Duration) error {
	url = strings.TrimPrefix(url, "http://")

	deadline := time.Now().Add(timeout)
	for time.Now().Before(deadline) {
		conn, err := net.Dial("tcp", url)
		if err != nil && strings.Contains(err.Error(), "connection refused") {
			continue
		} else if err != nil {
			return err
		}

		conn.Close()
		return nil
	}

	return fmt.Errorf("server didn't start-up in %s", timeout)
}

func getFreePort() (int, error) {
	from, to := 1024, 49151
	for port := from; port <= to; port++ {
		conn, err := net.Dial("tcp", fmt.Sprintf("127.0.0.1:%d", port))
		if err != nil {
			return port, nil
		}

		conn.Close()
	}

	return 0, fmt.Errorf("unable to find port in range [%d-%d]", from, to)
}
