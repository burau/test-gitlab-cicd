package main

import (
	"fmt"
	"net/http"
	"os"

	httpapi "gitlab.com/burau/test-gitlab-cicd/pkg/server/http"
)

const portEnv = "HTTP_PORT"

func main() {
	mux := http.NewServeMux()

	s := httpapi.NewServer()
	mux.HandleFunc("/hello", s.HelloWorld)

	url := fmt.Sprintf("127.0.0.1:%s", os.Getenv(portEnv))
	if err := http.ListenAndServe(url, mux); err != nil {
		panic(err)
	}
}
