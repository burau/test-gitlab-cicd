package http

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestNewServer(t *testing.T) {
	s := NewServer()
	if s == nil {
		t.Error("Server must be not nil")
		return
	}
}

func TestHelloWorld(t *testing.T) {
	s := NewServer()

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "http://example.com/hello", nil)

	s.HelloWorld(w, r)

	resp := w.Result()
	if resp.Body != nil {
		defer resp.Body.Close()
	}

	buf := new(bytes.Buffer)
	_, _ = buf.ReadFrom(resp.Body)

	expected := "{\"message\":\"Hello World of CI/CD\"}"
	result := buf.String()

	if result != expected {
		t.Errorf("Got unexpected result: result %q, got %q\n", result, expected)
		return
	}
}
