package http

import (
	"fmt"
	"net/http"
)

type Server struct{}

func NewServer() *Server {
	return &Server{}
}

func (s *Server) HelloWorld(w http.ResponseWriter, r *http.Request) {
	_, _ = fmt.Fprintf(w, "{\"message\":\"Hello World of CI/CD\"}")
}
