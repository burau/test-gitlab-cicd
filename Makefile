PACKAGES := $(shell go list ./... | grep -v /vendor/)

PWD := $(shell pwd)


testing:
	@go test -v --cover -race $(PACKAGES)

integration-testing:
	@go test -v --cover -tags=integration -race $(PACKAGES)

check-formmating:
	@test -z $(shell go fmt ${PACKAGES}) 

check-govet:
	@test -z $(shell go vet ${PACKAGES})

check-golangci:
	docker run --rm -v ${PWD}:/go/src/gitlab.com/burau/test-gitlab-cicd -w /go/src/gitlab.com/burau/test-gitlab-cicd golangci/golangci-lint:v1.39.0 golangci-lint run -c build/.golangci.yml ./...
